module github.com/andreimarcu/linx-server

go 1.14

require (
	github.com/GeertJohan/go.rice v1.0.2
	github.com/aws/aws-sdk-go v1.44.114
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/dchest/uniuri v1.2.0
	github.com/dustin/go-humanize v1.0.0
	github.com/flosch/pongo2 v0.0.0-20200913210552-0d938eb266f3
	github.com/gabriel-vasile/mimetype v1.4.1
	github.com/klauspost/cpuid/v2 v2.1.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.21
	github.com/minio/sha256-simd v1.0.0
	github.com/russross/blackfriday v1.6.0
	github.com/vharitonsky/iniflags v0.0.0-20180513140207-a33cd0b5f3de
	github.com/zeebo/bencode v1.0.0
	github.com/zenazn/goji v1.0.1
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a
	golang.org/x/net v0.0.0-20221012135044-0b7e1fb9d458 // indirect
	golang.org/x/sys v0.0.0-20221010170243-090e33056c14 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
